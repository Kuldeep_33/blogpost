package main

import (
	"github.com/Kdsingh333/Golang-BlogPost/models"
	"github.com/Kdsingh333/Golang-BlogPost/pkg/config"
	"github.com/Kdsingh333/Golang-BlogPost/pkg/database"
	logger "github.com/Kdsingh333/Golang-BlogPost/pkg/log"
	"github.com/Kdsingh333/Golang-BlogPost/routers"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		logger.Fatalf("config GetConfig() error :%s", err)
	}

	//Get DB connection
	db, err := database.GetDB(cfg)
	if err != nil {
		logger.Fatalf("Database err: %s", err)
	}
	app := config.App{
		Config: cfg,
		DB:     db,
	}

	err = db.AutoMigrate(models.GetMigrationModels()...)
	if err != nil {
		logger.Fatalf("database migration error:%s", err)
	}

	routers.SetupAndRunServer(&app)

}
