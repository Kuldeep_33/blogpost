package controllers

import (
	"net/http"

	"github.com/Kdsingh333/Golang-BlogPost/auth"
	"github.com/Kdsingh333/Golang-BlogPost/models"
	logger "github.com/Kdsingh333/Golang-BlogPost/pkg/log"
	"github.com/gin-gonic/gin"
)

func (base *BaseController) RegisterUser(ctx *gin.Context) {
	var (
		userRepo = models.InitUserRepo(base.DB)
		request  RegisterUser
	)

	err := ctx.ShouldBindJSON(&request)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Something went wrong1",
			"success": false,
		})

		logger.Warn(err)
		return
	}

	newUser := &models.User{
		Name:        request.Name,
		PhoneNumber: request.PhoneNumber,
		Password:    request.Password,
	}

	err = userRepo.CreateUser(newUser)
	if err != nil {
		logger.Error(err)
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": "something went wrong2",
			"success": false,
		})
		return
	}

	jwtToken, err := auth.GenerateJWT(newUser.PhoneNumber, newUser.ID)
	if err != nil {
		logger.Error(err)
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Something went wrong3",
			"success": false,
		})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"phone_number": newUser.PhoneNumber,
		"token":        jwtToken,
		"success":      true,
	})

}

// func ( base *BaseController) LogOut(context *gin.Context){
// 	bearerToken := context.GetHeader("Authorization")
// 	if bearerToken == "" {
// 		context.JSON(http.StatusUnauthorized, gin.H{
// 			"error": "request does not contain an access token",
// 		})
// 		context.Abort()
// 		return
// 	}
// 	token := strings.Split(bearerToken, "Bearer ")

// 	models.blacklistedKeys[token[1]] = true
// 	logger.Info("User successfully logout")
// 	context.JSON(http.StatusOK, gin.H{
// 		"msg": "User successfully logout",
// 	})
// }
