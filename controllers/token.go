package controllers

import (
	"net/http"

	"github.com/Kdsingh333/Golang-BlogPost/auth"
	"github.com/Kdsingh333/Golang-BlogPost/models"
	logger "github.com/Kdsingh333/Golang-BlogPost/pkg/log"
	"github.com/gin-gonic/gin"
)

func (base *BaseController) GenerateToke(ctx *gin.Context) {
	var (
		request  TokenRequest
		userRepo = models.InitUserRepo(base.DB)
	)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	user, err := userRepo.GetUser(request.PhoneNumber)
	if user.ID == 0 {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Invalid Phone number",
			"success": false,
		})
		return
	}

	if err != nil {
		logger.Error(err)
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"message": "something went wrong",
			"success": false,
		})
		return
	}

	credentialError := userRepo.CheckPassword(request.Password, user)
	if credentialError != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"error":   "Invalid Password",
			"success": false,
		})
		return
	}

	tokenString, err := auth.GenerateJWT(user.PhoneNumber, user.ID)
	if err!= nil{
		ctx.JSON(http.StatusInternalServerError,gin.H{
			"error":err.Error(),
		})

		ctx.Abort()
		return
	}
	
	
	ctx.JSON(http.StatusOK, gin.H{
		"token": tokenString,
		"success":true,
		"name": user.Name,
	}) 
}
