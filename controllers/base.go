package controllers

import (
	"github.com/Kdsingh333/Golang-BlogPost/pkg/config"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type BaseController struct {
	DB *gorm.DB
	Config config.Config
	Log *logrus.Logger
}