package routers

import (
	"github.com/Kdsingh333/Golang-BlogPost/auth"
	"github.com/Kdsingh333/Golang-BlogPost/controllers"
	"github.com/Kdsingh333/Golang-BlogPost/pkg/config"
	"github.com/Kdsingh333/Golang-BlogPost/routers/middleware"
	"github.com/sirupsen/logrus"
)

func RegisterRoutes(app config.App) {
	ctrl := controllers.BaseController{
		DB:     app.DB,
		Config: app.Config,
		Log:    logrus.New(),
	}

	apiGroup := app.Router.Group("/api")
	blogGroup := apiGroup.Group("/blog")
	userGroup := apiGroup.Group("/user")

	// user routes
	userGroup.POST("/token", ctrl.GenerateToke)
	userGroup.POST("/register", ctrl.RegisterUser)
	userGroup.POST("/logout",auth.LogOut)

	//blog routers

	blogGroup.Use(middleware.Auth(app.DB))
	blogGroup.GET("", ctrl.GetBlogs)
	blogGroup.POST("",ctrl.AddBlog)
	blogGroup.DELETE("/:id",ctrl.DeleteBlog)

}
