package routers

import (
	"net/http"

	"github.com/Kdsingh333/Golang-BlogPost/pkg/config"
	"github.com/Kdsingh333/Golang-BlogPost/routers/middleware"
	"github.com/gin-gonic/gin"
)

func SetupAndRunServer(app *config.App) {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(middleware.CORSMiddleware())
    
	app.Router = router  
	// Register routes
	registerRoutes(*app)   
	// Run server after InitRoutes
	router.Run(":5000");
}

func registerRoutes(app config.App){
	app.Router.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "Route Not Found"})
	})

	app.Router.GET("/health",func(ctx *gin.Context){
		ctx.JSON(http.StatusOK,gin.H{"live":"ok"})
	})
	RegisterRoutes(app)
}
