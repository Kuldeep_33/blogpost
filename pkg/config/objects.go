package config

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type App struct {
	Router *gin.Engine
	DB *gorm.DB
	Config Config
}
// Config structure we are using during creating connection and we are unmarshelling it with the help of vipher
type Config struct{
	Name string    `mapstructure:"DB_NAME"`
	Username string   `mapstructure:"DB_USER"`
	Password  string    `mapstructure:"DB_PASSWORD"`
	Host  string        `mapstructure:"DB_HOST"`
	Port  string        `mapstructure:"DB_PORT"`
	SSLMode string      `mapstructure:"DB_SSL_MODE"`
}