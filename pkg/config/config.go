package config

import (
	"path"
	"runtime"
	"strings"
     logger  "github.com/Kdsingh333/Golang-BlogPost/pkg/log"
	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
)

// using current working directory as the config directory
func getPwd() string{
	_,b,_,_ := runtime.Caller(0)
	return path.Join(path.Dir(path.Dir(path.Dir(b))))
}

//GetConfing configuration
func GetConfig()(Config,error){
	var cfg Config
    e:= enviper.New(viper.New());
	e.AddConfigPath(getPwd())
	e.SetConfigName(".env")
	e.SetConfigType("env")

	e.SetEnvKeyReplacer(strings.NewReplacer(".","_"))

	e.AutomaticEnv()

	if err:= e.ReadInConfig(); err!=nil{
		logger.Warn(err)
		if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
			logger.Fatal("Error reading config file: ", err)
		}
	} 

	err := e.Unmarshal(&cfg)

	if err!= nil{
		logger.Errorf("error to decode,%v",err)
		return cfg,nil
	}
	 
	return cfg,nil
}