package database

import (
	"fmt"
	"log"

	"github.com/Kdsingh333/Golang-BlogPost/pkg/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	db *gorm.DB
	err error
)

// connection create database connection

func GetDB(cfg config.Config)(*gorm.DB,error){
	DNS := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s",cfg.Host,cfg.Username,cfg.Password,cfg.Name,cfg.Port,cfg.SSLMode)
	db,err:=gorm.Open(postgres.Open(DNS),&gorm.Config{})

	if err!=nil{
		log.Fatal("Db connection error")
		return nil,err
	}

	return db,nil
}