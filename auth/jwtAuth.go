package auth

import (
	"errors"
	"strings"
	"time"

	logger "github.com/Kdsingh333/Golang-BlogPost/pkg/log"
	"github.com/golang-jwt/jwt/v4"

	"net/http"
	"github.com/gin-gonic/gin"
)

var jwtKey = []byte("supersecretKey")

var BlacklistedKeys = make(map[string]bool)


type JWTClaim struct {
	PhoneNumber int    `json:"phone_number"`
	UserId      uint64 `json:"user_id"`
	jwt.RegisteredClaims
}

func GenerateJWT(PhoneNumber int, userId uint64) (tokenString string, err error) {
	expirationTime := time.Now().Add(1 * time.Hour)
	claims := &JWTClaim{
		PhoneNumber: PhoneNumber,
		UserId:      userId,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err = token.SignedString(jwtKey)

	return
}

func ParseClaims(signedToken string) (int, uint64, error) {
	token, err := jwt.ParseWithClaims(signedToken, &JWTClaim{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtKey), nil
	})
	if err != nil {
		return 0, 0, err
	}

	claims, ok := token.Claims.(*JWTClaim)
	if ok && token.Valid {
		return claims.PhoneNumber, claims.UserId, nil
	} else {
		err = errors.New("token expired")
		return 0, 0, err
	}
}

func LogOut(context *gin.Context) {
	bearerToken := context.GetHeader("Authorization")
	if bearerToken == "" {
		context.JSON(http.StatusUnauthorized, gin.H{
			"error": "request does not contain an access token",
		})
		context.Abort()
		return
	}
	token := strings.Split(bearerToken, "Bearer ")

	BlacklistedKeys[token[1]] = true
	logger.Info("User successfully logout")
	context.JSON(http.StatusOK, gin.H{
		"msg": "User successfully logout",
	})
}
